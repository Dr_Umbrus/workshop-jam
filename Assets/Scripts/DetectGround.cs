﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectGround : MonoBehaviour
{
    CharacterController2D cc;

    private void Awake()
    {
        cc = GetComponentInParent<CharacterController2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Wall")
        {
            cc.JumpAgain();
        }
        
    }
}
