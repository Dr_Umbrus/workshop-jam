﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeText : MonoBehaviour
{
    [SerializeField]
    Text[] texts;
    Text message;
    public int numberText=-1;

    bool canSwap = true;

    public float lerpTime = 0;

    [SerializeField]
    Color flukolor;

    [SerializeField]
    GameObject timerPar;

    [SerializeField]
    Text classement, timerText, finalRankText;

    GameManager man;
    int classementInt = 10;

    [SerializeField]
    ScriptableColor scoreColors;

    private void Awake()
    {
        man = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.anyKeyDown && canSwap)
        {
            if (numberText < 4)
            {
                numberText++;
                canSwap = false;
                lerpTime = 0;
            }

            if (numberText == 2)
            {
                
                if (man.timeAttack)
                {
                    timerPar.SetActive(true);
                    classementInt = CalculateScore();
                }
                else
                {
                    classementInt = 10;
                }

                string minutes = ((int)man.currentTime / 60).ToString();
                string secondes = (man.currentTime % 60).ToString("f2");

                timerText.text = "Final Time = " + minutes + ":" + secondes;
                classement.text = classementInt.ToString(); 
            }
        }

        lerpTime += Time.deltaTime;

        if(lerpTime>=1 && !canSwap)
        {
            canSwap = true;
            
        }

        if (numberText < 0)
        {
            texts[numberText + 1].color = Color.Lerp(Color.clear, Color.white, lerpTime);
        }
        else if (numberText < 2)
        {
            texts[numberText].color = Color.Lerp(Color.clear, Color.white, 1-lerpTime);
            texts[numberText + 1].color = Color.Lerp(Color.clear, Color.white, lerpTime);
        }
        else if(numberText<3)
        {
            texts[numberText].color = Color.Lerp(Color.white, Color.clear, lerpTime);
            texts[numberText + 1].color = Color.Lerp(Color.clear, flukolor, lerpTime);

            classement.color = Color.Lerp(Color.clear, Color.white, lerpTime);
            timerText.color = Color.Lerp(Color.clear, Color.white, lerpTime);
            finalRankText.color = Color.Lerp(Color.clear, scoreColors.tileMap[classementInt], lerpTime);
        }
        else
        {
            texts[numberText].color = Color.Lerp(flukolor, Color.clear, lerpTime);

            classement.color = Color.Lerp(Color.clear, Color.white, 1 - lerpTime);
            timerText.color = Color.Lerp(Color.clear, Color.white, 1 - lerpTime);
            finalRankText.color = Color.Lerp(scoreColors.tileMap[classementInt], Color.clear, lerpTime);
        }

        if(lerpTime>1 && numberText >=3)
        {
            man.ChangeScene("Menu");
        }
    }

    int CalculateScore()
    {
        int minScore = 10;

        for (int i = 0; i < man.times.Length; i++)
        {
            if (man.times[i] == man.currentTime)
            {
                minScore = i;
            }
        }

        return minScore;
    }
}
