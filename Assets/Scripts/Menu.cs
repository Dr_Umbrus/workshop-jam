﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    GameManager man;

    [SerializeField]
    GameObject mainMenu, credits, levelSelect, leaderboard;

    [SerializeField]
    GameObject[] levels;

    [SerializeField]
    Text epi, azerTex;

    [SerializeField]
    Text[] scores;

    private void Awake()
    {
        man = FindObjectOfType<GameManager>();

        if (man.azer)
        {
            azerTex.text = "AZERTY Mode ENABLED";
        }
        else
        {
            azerTex.text = "QWERTY Mode ENABLED";
        }

        if (man.epilepsy)
        {
            epi.text = "Epilepsy safe mode ENABLED";
        }
        else
        {
            epi.text = "Epilepsy safe mode DISABLED";
        }
    }

    public void LoadGame(int level)
    {
        man.currentLevel = level;
        man.NextLevel();
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void GoCredit()
    {
        mainMenu.SetActive(false);
        credits.SetActive(true);
        levelSelect.SetActive(false);
        leaderboard.SetActive(false);
    }

    public void GoChoice()
    {
        mainMenu.SetActive(false);
        credits.SetActive(false);
        leaderboard.SetActive(false);
        levelSelect.SetActive(true);
        for (int i = 1; i < levels.Length; i++)
        {
            if (i < man.maxVisitedLevel)
            {
                levels[i].SetActive(true);
            }
            else
            {
                levels[i].SetActive(false);
            }
        }
    }

    public void GoLead()
    {
        mainMenu.SetActive(false);
        credits.SetActive(false);
        levelSelect.SetActive(false);
        leaderboard.SetActive(true);

        for (int i = 0; i < man.times.Length; i++)
        {

            string minutes = ((int)man.times[i] / 60).ToString();
            string secondes = (man.times[i] % 60).ToString("f2");
            scores[i].text = (i+1) + " = " +minutes + ":" + secondes;
        }
    }

    public void BackMenu()
    {
        mainMenu.SetActive(true);
        credits.SetActive(false);
        levelSelect.SetActive(false);
        leaderboard.SetActive(false);
    }

    public void Ep()
    {
        man.epilepsy = !man.epilepsy;


        if (man.epilepsy)
        {
            epi.text = "Epilepsy safe mode ENABLED";
        }
        else
        {
            epi.text = "Epilepsy safe mode DISABLED";
        }
    }

    public void Nuke()
    {
        man.DelSave();

        for (int i = 0; i < man.times.Length; i++)
        {

            string minutes = ((int)man.times[i] / 60).ToString();
            string secondes = (man.times[i] % 60).ToString("f2");
            scores[i].text = (i + 1) + " = " + minutes + ":" + secondes;
        }
    }

    public void Az()
    {
        man.azer = !man.azer;

        if (man.azer)
        {
            azerTex.text = "AZERTY Mode ENABLED";
        }
        else
        {
            azerTex.text = "QWERTY Mode ENABLED";
        }
    }
}
