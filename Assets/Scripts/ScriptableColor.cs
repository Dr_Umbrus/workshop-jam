﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Permet d'être créé dans les assets en faisant click droit + create.
[CreateAssetMenu]

//Un scriptable object pour stocker une couleur. Pourrait servir de palette en mettant un array à la place.
public class ScriptableColor : ScriptableObject
{
    /// <summary>
    /// Ordre des couleurs
    /// 
    /// 0 = Extérieur des plateformes
    /// 1 = Intérieur des plateformes.
    /// 2 = Outline des plateformes
    /// 3 = Character
    /// 4 = Collines
    /// 5 = Arbres décor
    /// 6 = Arbres mouvants
    /// 7 = Nuages
    /// 8 = Lune
    /// </summary>
    /// 

    public Color[] decor;

    [ColorUsage(true, true)]
    public Color[] tileMap;
}
