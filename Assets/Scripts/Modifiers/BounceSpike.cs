﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceSpike : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<CharacterController2D>())
        {
            collision.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 1500);
            collision.GetComponent<Animator>().SetTrigger("Jump");
        }
    }
}
