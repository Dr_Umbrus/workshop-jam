﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrollEnding : MonoBehaviour
{
    [SerializeField]
    GameObject next;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<CharacterController2D>())
        {
            next.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
