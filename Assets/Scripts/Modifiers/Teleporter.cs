﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    [SerializeField]
    Teleporter otherEnd;

    bool canTel = true;

    public void RestartTel()
    {
        canTel = false;
        StartCoroutine(WaitTime());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<CharacterController2D>() && canTel)
        {
            RestartTel();
            otherEnd.RestartTel();
            collision.transform.position = otherEnd.transform.position;
        }
    }

    IEnumerator WaitTime()
    {
        yield return new WaitForSeconds(0.2f);
        canTel = true;
    }
}
