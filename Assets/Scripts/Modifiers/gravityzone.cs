﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gravityzone : MonoBehaviour
{
    public int value;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<CharacterController2D>())
        {
            if (collision.GetComponent<Rigidbody2D>().gravityScale != value)
            {
                //collision.GetComponent<Animator>().SetTrigger("Jump");
                collision.GetComponent<CharacterController2D>().grounded = false;
            }
            
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<CharacterController2D>())
        {
            collision.GetComponent<Rigidbody2D>().gravityScale = value;
        }
    }
}
