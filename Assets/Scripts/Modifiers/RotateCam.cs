﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCam : MonoBehaviour
{
   public Transform target;
    public float startX;
    public float speed;

    private void Update()
    {
        transform.Rotate(new Vector3(0, 0, target.position.x - startX) * Time.deltaTime*speed);
    }


}
