﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetParticleColour : MonoBehaviour
{
    [SerializeField]
    int whichCol = 0;

    void Awake()
    {
        GameManager man = FindObjectOfType<GameManager>();

        ParticleSystem ps = GetComponent<ParticleSystem>();
        var main = ps.main;

        main.startColor = man.backgroundColors[man.currentLevel-1].tileMap[whichCol];
    }

    
}
