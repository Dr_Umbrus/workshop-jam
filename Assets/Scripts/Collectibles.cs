﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectibles : MonoBehaviour
{
    [SerializeField]
    GameObject particles;

    Door porte;

    AudioSource aud;

    [SerializeField]
    AudioClip[] grabClip;

    private void Awake()
    {
        porte = FindObjectOfType<Door>();
        aud = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<CharacterController2D>())
        {
            if (collision.GetComponent<CharacterController2D>().canMove)
            {
                Instantiate(particles, transform.position, Quaternion.identity);
                porte.RemoveCol();
                StartCoroutine(WaitDisappear());
            }
        }
    }


    IEnumerator WaitDisappear()
    {
        int rand = Random.Range(0, grabClip.Length);
        aud.clip = grabClip[rand];
        GetComponent<BoxCollider2D>().enabled=false;
        GetComponent<SpriteRenderer>().color = Color.clear;
        aud.Play();
        while (aud.isPlaying)
        {
            yield return 0;
        }

        gameObject.SetActive(false);
    }
}
