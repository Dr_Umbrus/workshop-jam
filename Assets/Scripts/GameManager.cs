﻿using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public bool epilepsy = true;
    
    public int currentLevel = 0;

    public int maxVisitedLevel = 0;

    [SerializeField]
    GameObject particlesEnd;

    [SerializeField]
    int maxLevel = 2;

    [SerializeField]
    Material material;

    [SerializeField]
    Material[] decors;

    public ScriptableColor[] backgroundColors;

    public bool levelEnd = false;

    [SerializeField]
    Text winText, congratText;

    [SerializeField]
    GameObject winTextParent, timeUI;

    [SerializeField]
    string[] winStrings;

    [SerializeField]
    AudioSource music, endLevel;

    public float currentTime=0;

    [SerializeField]
    Text levelTimeUI;

    public bool timeAttack = false;

    public bool azer = false;

    public float[] times= new float[10];


    //Singleton
    private static GameManager _instance;

    public static GameManager Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        DontDestroyOnLoad(gameObject);


        for (int i = 0; i < 10; i++)
        {
            times[i] = 99999999;
        }

        Load();
    }

    private void Update()
    {
        string minutes = ((int) currentTime / 60).ToString();
        string secondes = (currentTime %60).ToString("f2");

        levelTimeUI.text = "Current time = "+ minutes + ":" + secondes; 

        if (Input.GetKeyDown(KeyCode.Escape) && !levelEnd)
        {
            ChangeScene("Menu");
        }

        if (Input.anyKeyDown && levelEnd)
        {
            
            FindObjectOfType<CameraTransition>().StartRot();
        }

        if (!levelEnd && currentLevel>0)
        {
            currentTime += Time.deltaTime;
        }
    }

    public void NextLevel()
    {

        currentLevel++;

        if (currentLevel > maxVisitedLevel)
        {
            maxVisitedLevel++;
        }

        if (currentLevel == 1)
        {
            currentTime = 0;

            timeAttack = true;
        }

        if (currentLevel < maxLevel)
        {
            if (!music.isPlaying)
            {
                music.Play();
            }
            material.SetColor("_maincol", backgroundColors[currentLevel-1].tileMap[0]);
            material.SetColor("_secondcol", backgroundColors[currentLevel-1].tileMap[1]);
            material.SetColor("_thirdcol", backgroundColors[currentLevel-1].tileMap[2]);

            for (int i = 0; i < decors.Length; i++)
            {
                decors[i].SetColor("_Color", backgroundColors[currentLevel-1].decor[i]);
            }
            ChangeScene("level" + currentLevel);
        }
        else
        {
            ChangeScene("End Scene");
        }
    }

    public void EndLevel()
    {
        if (!levelEnd)
        {
            Instantiate(particlesEnd, new Vector3(0, -10, 0), Quaternion.identity);
            winTextParent.SetActive(true);
            winText.text = winStrings[currentLevel-1];
            winText.color = congratText.color = backgroundColors[currentLevel - 1].tileMap[2];
            levelEnd = true;
            endLevel.Play();
        }
        
    }

    public void ChangeScene(string sceneName)
    {
        timeUI.SetActive(false);
        winTextParent.SetActive(false);
        levelEnd = false;
        if (sceneName=="Menu" || sceneName == "End Scene")
        {
            timeAttack = false;
            music.Stop();
            
            currentLevel = -1;
        }

        if(sceneName== "End Scene")
        {
            SetLeaderBoard();
        }
        SceneManager.LoadScene(sceneName);

    }

    void SetLeaderBoard()
    {
        bool ok = false;
        for (int i = 0; i < times.Length; i++)
        {
            if(!ok && currentTime < times[i])
            {
                for (int j = times.Length-1; j >i; j--)
                {
                    times[j] = times[j - 1];
                }

                ok = true;
                times[i] = currentTime;
            }
        }

        Save();
    }

    
    void Save()
    {
        SaveTimes.SaveUnlock(this);
    }

    void Load()
    {
        float[] tempTime= SaveTimes.LoadUnlock();

        if (tempTime != null)
        {
            times = tempTime;
        }

    }

    public void DelSave()
    {
        for (int i = 0; i < 10; i++)
        {
            times[i] = 99999999;
        }

        Save();
    }

    public void EnableTime()
    {
        if (timeAttack)
        {
            timeUI.SetActive(true);
        }
    }
}
