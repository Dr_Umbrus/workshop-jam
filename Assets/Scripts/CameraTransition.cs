﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Rendering;


public class CameraTransition : MonoBehaviour
{
    bool rot = false;
    bool lerpUp = false;

    bool ended = false;

    bool active = true;

    Volume vol;
    [SerializeField]
    float maxSpeed, maxGlow, startGlow=6, lerpSpeed=1, normalLen=0;
    [SerializeField]
    float lerpTime = 1;

    UnityEngine.Rendering.Universal.Bloom blo;
    UnityEngine.Rendering.Universal.LensDistortion len;
    UnityEngine.Rendering.Universal.ChromaticAberration chro;

    GameManager man;


    // Start is called before the first frame update
    void Awake()
    {
        vol = GetComponent<Volume>();
        vol.profile.TryGet(out blo);
        vol.profile.TryGet(out len);
        vol.profile.TryGet(out chro);
        man = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            if (lerpUp)
            {
                lerpTime += Time.deltaTime * lerpSpeed;
            }
            else
            {
                lerpTime -= Time.deltaTime;
            }

            if (rot)
            {
                transform.Rotate(new Vector3(0, 0, Mathf.Lerp(0, maxSpeed, lerpTime)));
            }

            if (lerpTime < 1)
            {
                blo.intensity.Override(Mathf.Lerp(startGlow, maxGlow, lerpTime));
            }
            else
            {
                blo.intensity.Override(Mathf.Lerp(maxGlow, 10000, lerpTime - 1));
                blo.tint.Override(Color.white);
            }

            if (lerpUp)
            {
                len.intensity.Override(Mathf.Lerp(normalLen, -1, lerpTime * 2));
                chro.intensity.Override(Mathf.Lerp(0.1f, 1, lerpTime * 2));
            }
            else
            {
                len.intensity.Override(Mathf.Lerp(normalLen, 1, lerpTime));
                chro.intensity.Override(Mathf.Lerp(0.1f, 1, lerpTime));
            }

            if (lerpTime > 1 && lerpUp && !ended)
            {
                ended = true;
                man.NextLevel();
            }
        }
        
        

        if (lerpTime < 0 && !lerpUp)
        {
            active = false;
            man.EnableTime();
        }
    }

    public void StartRot()
    {
        if (man.epilepsy)
        {
            man.NextLevel();
        }
        if (!rot)
        {
            active = true;
            lerpTime = 0;
            rot = true;
            lerpUp = true;
        }
        
    }

    public void Lvl15()
    {
        active = true;
        lerpTime = 0.75f;
    }
}
