﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController2D : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer outline;

    [SerializeField]
    float speed=20;

    public float jumpForce=1000;

    public Rigidbody2D rb;

    public SpriteRenderer sr;

    public bool grounded = false;

    public bool canMove = true;

    [SerializeField]
    bool canJump = true;

    [SerializeField]
    int nJump = -1;

    [SerializeField]
    bool canLeft = true;

    public Animator anim;

    [SerializeField]
    GameObject colDet;

    [SerializeField]
    Vector3 downCol, upCol;


    public GameObject partDeath, partLand;


    public AudioSource jump, ground, death;

    public AudioClip[] jumpClips, deathClips;

    bool az = false;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        az = FindObjectOfType<GameManager>().azer;
    }


    private void FixedUpdate()
    {
        outline.sprite = sr.sprite;
        if (rb.gravityScale < 0)
        {
            colDet.transform.position = transform.position + upCol;
            if (!sr.flipY)
            {
                sr.flipY = true;
                outline.flipY = true;
            }

        }
        else
        {
            colDet.transform.position = transform.position + downCol;
            if (sr.flipY)
            {
                sr.flipY = false;
                outline.flipY = false;
            }
        }
        if (canMove)
        {
            Move();


        }
    }

    private void LateUpdate()
    {

        
        if (canMove)
        {

            if (grounded && Input.GetKeyDown(KeyCode.Space) && canJump)
            {
                if (nJump != 0)
                {
                    Jump();
                }
                
            }

            
        }
        
    }

    void Move()
    {
        float h = 0;

        if (az)
        {
            h = Input.GetAxis("Horizontal2");
        }
        else
        {
            h = Input.GetAxis("Horizontal");
        }
        
        anim.SetFloat("Move", Mathf.Abs(h));

        if(!canLeft && h < 0)
        {
            h = 0;
        }

        if (h < 0)
        {
            if (!sr.flipX)
            {
                sr.flipX = true;
                outline.flipX = true;
            }
        }
        else if(h>0)
        {
            if (sr.flipX)
            {
                sr.flipX = false;
                outline.flipX = false;
            }
        }

        transform.Translate(new Vector2(h, 0) * speed * Time.deltaTime);
    }

    public virtual void Jump()
    {
        int rand = Random.Range(0, jumpClips.Length);
        jump.clip = jumpClips[rand];
        jump.Play();
        anim.SetTrigger("Jump");
        nJump--;
        grounded = false;

        if (rb.gravityScale > 0)
        {
            rb.AddForce(Vector2.up * jumpForce);
        }
        else
        {
            rb.AddForce(Vector2.up * jumpForce *-1);
        }
        
    }

    public void JumpAgain()
    {
        if (!grounded)
        {
            ground.Play();
            anim.SetTrigger("Ground");
            grounded = true;
            Instantiate(partLand, colDet.transform.position, Quaternion.identity);
            rb.velocity = Vector2.zero;
        }
        
    }

    public void Death()
    {
        int rand = Random.Range(0, deathClips.Length);
        death.clip = deathClips[rand];
        death.Play();
        anim.SetTrigger("Ded");
        canMove = false;
        sr.color = Color.grey;
        transform.Translate(new Vector3(0, 0, 5));
        Instantiate(partDeath, transform.position, Quaternion.identity);
    }
}
