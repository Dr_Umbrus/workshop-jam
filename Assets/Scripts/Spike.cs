﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour
{
    SpawnerCharacter sc;

    private void Start()
    {
        sc = FindObjectOfType<SpawnerCharacter>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<CharacterController2D>())
        {
            if (collision.GetComponent<CharacterController2D>().canMove)
            {
                if (sc == null)
                {
                    sc = FindObjectOfType<SpawnerCharacter>();
                }
                collision.GetComponent<CharacterController2D>().Death();
                sc.Summon();
            }
            
        }
    }
}
