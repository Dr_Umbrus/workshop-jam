﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerCharacter : MonoBehaviour
{
    public GameObject player;

    public void Summon()
    {
        GameObject temp = Instantiate(player, transform.position, Quaternion.identity);

        RotateCam potato = FindObjectOfType<RotateCam>();
        if (potato != null)
        {
            potato.target = temp.transform;
        }
    }
}
