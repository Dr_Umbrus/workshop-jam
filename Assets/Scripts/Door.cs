﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField]
    int nCol=5;

    public void RemoveCol()
    {
        nCol--;
        if (nCol <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}
