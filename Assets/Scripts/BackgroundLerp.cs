﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundLerp : MonoBehaviour
{
    [SerializeField]
    float lerpTime = 0, speed;

    [SerializeField]
    Vector3 startPos, endPos;

    void Update()
    {
        lerpTime += Time.deltaTime*speed;

        if (lerpTime > 1)
        {
            lerpTime = 0;
        }

        transform.position = Vector3.Lerp(startPos, endPos, lerpTime);
    }
}
