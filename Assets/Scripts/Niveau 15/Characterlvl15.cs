﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Characterlvl15 : CharacterController2D
{
    bool walking = false;
    [SerializeField]
    GameObject walkPart;

    CameraTransition ct;

    Screnshake ss;

    private void Awake()
    {
        ss = FindObjectOfType<Screnshake>();
        ct = FindObjectOfType<CameraTransition>();
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        canMove = true;
    }

    void Update()
    {
        if (canMove)
        {
            if (anim.GetFloat("Move") < .3f || !grounded)
            {
                walking = false;
            }

            if (!walking && anim.GetFloat("Move") > .3f && grounded)
            {
                walking = true;
                StartCoroutine(Boomwalk());
            }
        }
        
    }

    IEnumerator Boomwalk()
    {
        yield return new WaitForSeconds(0.1f);
        Instantiate(walkPart, transform.position, Quaternion.identity);
        ss.StartShaking(.25f, 150f, 2f);
        
        if(canMove && anim.GetFloat("Move") > .3f && grounded)
        {
            StartCoroutine(Boomwalk());
        }

    }

    public override void Jump()
    {
        //ct.Lvl15();
        int rand = Random.Range(0, jumpClips.Length);
        jump.clip = jumpClips[rand];
        jump.Play();
        anim.SetTrigger("Jump");
        grounded = false;
        rb.AddForce(Vector2.up * jumpForce);
    }

    new public void Death()
    {
        int rand = Random.Range(0, deathClips.Length);
        death.clip = deathClips[rand];
        death.Play();
        anim.SetTrigger("Ded");
        canMove = false;
        sr.color = Color.grey;
        transform.Translate(new Vector3(0, 0, 5));
        Instantiate(partDeath, transform.position, Quaternion.identity);
        StopAllCoroutines();
    }
}
