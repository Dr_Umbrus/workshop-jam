﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveTimes
{

    public static void SaveUnlock(GameManager lb)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream stream = new FileStream(Application.persistentDataPath + "/save.sav", FileMode.Create);
        LeaderData ud = new LeaderData(lb);

        bf.Serialize(stream, ud);
        stream.Close();
    }

    public static float[] LoadUnlock()
    {
        if (File.Exists(Application.persistentDataPath + "/save.sav"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream stream = new FileStream(Application.persistentDataPath + "/save.sav", FileMode.Open);
            LeaderData ud = bf.Deserialize(stream) as LeaderData;

            stream.Close();

            return ud.times;
        }
        else
        {

            return null;
        }
    }
}

[Serializable]
public class LeaderData
{
    public float[] times = new float[10];

    public LeaderData(GameManager lb)
    {
        times = lb.times;
    }
}